public class GamerManager{
	Deck drawPile;
	Card centerCard;
	Card playerCard;
	
	public GamerManager(){
		this.drawPile = new Deck();
		this.drawPile.shuffle();
		this.centerCard = drawPile.drawTopCard();
		this.playerCard = drawPile.drawTopCard();
	}
	//toString method
	public String toString (){
		String builder = "";
		for(int i=0;i<20;i++){
			builder += "***";
		}
		builder += "\n Center card: " +this.centerCard.toString()+ "\n";
		builder+= "Player card: "+ this.playerCard.toString()+ "\n";
		for(int i=0;i<20;i++){
			builder += "--";
		}
		return builder;
	}
	//dealCard method
	public void dealCard(){
		this.drawPile.shuffle();
		this.centerCard = drawPile.drawTopCard();
		this.playerCard = drawPile.drawTopCard();
		
	}
	//getNbOfCards method
	public int getNbOfCards(){
		return this.drawPile.length();
	}
	//calPoints method
	public int calPoints(){
		if(this.playerCard.getValue().equals(this.centerCard.getValue())){
			return 4;
		}else if (this.playerCard.getSuit().equals(this.centerCard.getSuit())){
			return 2;
		}else{
			return -1;
		}
	}
	
}